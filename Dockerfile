FROM php:apache

RUN apt update &&\
    apt upgrade -y &&\ 
    apt install -y vim 

#VIM als editor um configs zu bearbeiten

RUN a2enmod rewrite
RUN a2enmod ssl

COPY server.crt /etc/apache2/ssl/server.crt
COPY server.key /etc/apache2/ssl/server.key
COPY dev.conf /etc/apache2/sites-enabled/dev.conf

RUN service apache2 restart

EXPOSE 80
EXPOSE 443
